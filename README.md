# README #

The Apache Common collection for Java has a very nice library called "BiDiMap",
and to my surprise I found out that Python lacks this,
even though it is trivial to implement.
So, I did just that.

### What is this repository for? ###

* bidimap introduces a class, BiDiMap, that works just like the Java BiDiMap:
  a bidirectional mapping, where both directions are equally fast.
* Version: 0.9

### How do I get set up? ###

* It is just a single file library: put it somewhere where "import" reaches it,
* and say "from bidimap import BiDiMap". You're ready to go.
* Configuration: nothing to configure
* Dependencies: no dependencies
* How to run tests: run "bidimap.py" from the command line

### Contribution guidelines ###

* I would really like this to become part of the Python standard libraries,
  so if you have tips/hints, let me know

### Who do I talk to? ###

* Jurjen N.E. Bos
* That's "jneb" at bugs.python.org