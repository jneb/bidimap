#!python3
"""A BiDiMap, based on
https://commons.apache.org/proper/commons-collections/apidocs/org/apache/commons/collections4/BiDiMap.html
and modified where needed to be Pythonic.
This class behaves just like a dict for almost all operations.
From an efficiency point of view, this class is useful if you do a lot of
lookup style operations on the reverse map. The following operations are more
efficient than the equivalent piece of code on a dict:
    getKey
    inverseBiDiMap
    removeValue
If you do many of these operations on a big dict, BiDiMap may be what you need.

The following operations on dict make no sense and give an error:
    fromkeys
    setdefault
    pop(key) of non-existing key
"""

class BiDiMap(dict):
    """A bidirectional mapping.
    Behaves like a dict, but also allows reverse lookups.
    Operations on inverse map are (up to a constant) equally fast
    both ways.
    >>> tel = BiDiMap({'jack': 4098, 'sape': 4139})
    >>> tel['guido'] = 4127
    >>> tel
    BiDiMap({'jack': 4098, 'sape': 4139, 'guido': 4127})
    >>> tel['jack']
    4098
    >>> 'guido' in tel
    True
    >>> tel['irv'] = 4127
    Traceback (most recent call last):
        ...
    ValueError: Value already exists in BiDiMap: 4127
    >>> list(tel)
    ['jack', 'sape', 'guido']
    >>> # operations below use the reverse map for speed
    >>> tel.inverseBiDiMap()
    BiDiMap({4098: 'jack', 4139: 'sape', 4127: 'guido'})
    >>> 4139 in tel.values()
    True
    >>> tel.getKey(4127)
    'guido'
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # build the inverse map
        self._inverse = {value:key
                         for key, value in self.items()}
        # check for bijection
        if len(self._inverse) != len(self):
            raise ValueError("Double value encountered")

    def __repr__(self):
        return f'BiDiMap({super().__repr__()})'

    # additional functions; names from Jave interface spec
    # put is omitted because it is equivalent to __setitem__
    def getKey(self, value):
        """Lookup value from key.
        Equivalent to next(k for k,v in self.items() if v==value)
        but runs in O(log(n)) instead of O(n)
        """
        return self._inverse[value]

    def inverseBiDiMap(self):
        """Return inverse map in O(1)
        Equivalent to dict((v,k) for k,v in self.items())
        but runs in O(1) instead of O(n)
        """
        # initialize BiDiMap skipping __init__
        result = dict.__new__(self.__class__)
        # set dictionary and inverse without computation
        dict.update(result, self._inverse)
        result._inverse = dict(self)
        return BiDiMap(result)

    def removeValue(self, value):
        """Remove item by value
        Equivalent to del self[next(k for k,v in self.items() if v==value)]
        but runs in O(log(n)) instead of O(n)
        """
        # we let __del__ do the work for us
        del self[self._inverse[value]]


    # overridden methods to make all this work like a normal dict
    def __setitem__(self, key, value):
        if value in self._inverse:
            if self._inverse[value] == key:
                # allow repeated assignment
                return
            raise ValueError("Value already exists in BiDiMap: " + str(value))
        # remove old binding of value if needed
        try:
            del self._inverse[self[key]]
        except KeyError:
            pass
        # set link both ways
        super().__setitem__(key, value)
        self._inverse[value] = key

    def __delitem__(self, key):
        value = self[key]
        super().__delitem__(self, key)
        del self._inverse[value]

    @staticmethod
    def fromkeys(iterable, value=None):
        raise AttributeError("BiDiMap.fromkeys() is not allowed")

    def pop(self, key):
        """Equivalent to dict.pop()
        Raises KeyError if key not in self: default argument not supported.
        """
        result = super().pop(key)
        del self._inverse[result]
        return result

    def popitem(self):
        key, value = super().popitem()
        del self._inverse[value]
        return key, value

    def setdefault(self, iterable, value=None):
        raise AttributeError("BiDiMap.setdefault is not allowed")

    def update(self, other, check_doubles=True):
        """Update map with other map.
        If the resulting mapping would get double entries,
        a ValueError is raised, but the effect on self is undefined!
        """
        super().update(other)
        if isinstance(other, BiDiMap):
            self._inverse.update(other._inverse)
        else:
            self._inverse.update(
                (value, key)
                for key, value in other.items()
                )
        # check invariant
        if len(self._inverse) != len(self):
            # prevent accidental use of this broken object
            del self._inverse
            raise ValueError("Double value encountered after update")

__test__ = {
'init': '''
    >>> b = BiDiMap([(1,2), (3,4)])
    >>> b.inverseBiDiMap()
    BiDiMap({2: 1, 4: 3})
    >>> BiDiMap.fromkeys([(1,2), (3,4)])
    Traceback (most recent call last):
        ...
    AttributeError: BiDiMap.fromkeys() is not allowed
    >>> b.setdefault(5, 2)
    Traceback (most recent call last):
        ...
    AttributeError: BiDiMap.setdefault is not allowed
    >>> b = BiDiMap(enumerate("Hallo"))
    Traceback (most recent call last):
        ...
    ValueError: Double value encountered
    >>> 
''',
'pop': '''
    >>> b = BiDiMap([(1,2), (3,4)])
    >>> b.pop(1)
    2
    >>> b.pop(1)
    Traceback (most recent call last):
        ...
    KeyError: 1
''',
'check': '''
    >>> b = BiDiMap([(1,2), (3,4)])
    >>> b[1] = 3
    >>> b[1] = 2
    >>> b[1] = 4
    Traceback (most recent call last):
        ...
    ValueError: Value already exists in BiDiMap: 4
''',
'update': '''
    >>> w = BiDiMap(enumerate('word'))
    >>> w.update(BiDiMap(enumerate('second', start=4)))
    Traceback (most recent call last):
        ...
    ValueError: Double value encountered after update
    >>> w = BiDiMap(enumerate('word'))
    >>> w.update(BiDiMap(enumerate('baz', start=4)))
    >>> w
    BiDiMap({0: 'w', 1: 'o', 2: 'r', 3: 'd', 4: 'b', 5: 'a', 6: 'z'})
''',
}

if __name__=='__main__':
    import doctest
    doctest.testmod()
